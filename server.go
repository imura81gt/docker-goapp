package main

import (
	"github.com/go-martini/martini"
	"os"
)

func main() {
	hostname, err := os.Hostname()

	if err != nil {
		panic(err)
	}
	m := martini.Classic()
	m.Get("/", func() string {
		return "golang:" + hostname
	})
	m.Run()
}
